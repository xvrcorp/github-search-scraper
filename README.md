# Github Scraper

Github search Scraper

## Input

- Keywords list
- Proxies list
- Search type (Repositories, Issues, Wikis)

````
{
  "keywords": [
    "openstack",
    "nova",
    "css"
  ],
  "proxies": [
    "194.126.37.94:8080",
    "13.78.125.167:8080"
  ],
  "type": "Repositories"
}
```

### Run

````

python main.py

```

### Output

Search results JSON

```

[
{
"url": "https://github.com/atuldjadhav/DropBox-Cloud-Storage"
}
]

```

Search extra results JSON

```

[
{
"extra": {
"language_stats": {
"Python": 100.0
},
"owner": "GetBlimp"
},
"url": "https://github.com/GetBlimp/django-rest-framework-jwt"
},
{
"extra": {
"language_stats": {
"Makefile": 3.4,
"Python": 96.6
},
"owner": "lock8"
},
"url": "https://github.com/lock8/django-rest-framework-jwt-refresh-token"
}
...
]

```

## tests

```

python -m pytest

```
## Author

- **Javier Rodriguez** - [LinkedIn](https://www.linkedin.com/in/javierrodriguezb/)

## License

This project is licensed under the MIT License
```
