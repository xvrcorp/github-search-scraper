import urllib.parse
from urllib.parse import urljoin, urlparse, urlunparse, parse_qsl
from bs4 import BeautifulSoup

import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import RetryError, ProxyError
from urllib3.util import Retry
from urllib3.exceptions import MaxRetryError


class GithubScraper():
    def __init__(self, proxy):
        self.__proxies = {'http': proxy, 'https': proxy}
        self.__url_base = 'https://github.com'
        self.__max_retries = 5
        self.__headers = {
            'User-Agent':
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
        }

    def __run(self, method, url, params):
        if not url:
            raise Exception("URL is required")

        session = requests.Session()
        retries = Retry(
            total=self.__max_retries,
            backoff_factor=0.1,
            status_forcelist=[500, 502, 503, 504])
        session.mount('http://', HTTPAdapter(max_retries=retries))

        content = None

        try:
            response = session.request(
                method,
                url,
                params=params,
                proxies=self.__proxies,
                headers=self.__headers)
            content = response.content
        except (MaxRetryError, RetryError, ProxyError) as error:
            print(f'Error: {error}')

        return content

    def __extract_links(self, html):
        links = []
        if (html):
            soup = BeautifulSoup(html, 'html.parser')
            a_tags = soup.find_all(attrs={'data-hydro-click': True})

            links = list(
                map(lambda a_tag: {'url': urljoin(self.__url_base, a_tag['href'])},
                    a_tags))

        return links

    def __extract_links_extra(self, html):

        links = []
        if (html):

            soup = BeautifulSoup(html, 'html.parser')
            lis = soup.select('li.repo-list-item')

            for li in lis:

                # Repository url
                repository_url = urljoin(self.__url_base, li.find('a')['href'])

                # Extract owner
                owner = urlparse(repository_url).path.split('/')[1]

                # Extract repository name id
                repository_name_id = urlparse(repository_url).path.split(
                    '/')[2]

                # Extract language stats
                language_stats = self.__extract_language_stats(
                    owner, repository_name_id)

                links.append({
                    'url': repository_url,
                    'extra': {
                        'owner': owner,
                        'language_stats': language_stats
                    }
                })

        return links

    def __extract_language_stats(self, owner, repository_name_id):
        language_stats = {}

        url_parts = list(urlparse(self.__url_base))

        # Add repository path
        url_parts[2] = f'{owner}/{repository_name_id}'

        repository_html = self.__run('GET', urlunparse(url_parts), {})

        if (repository_html):
            soup = BeautifulSoup(repository_html, 'html.parser')

            languages = soup.findAll("span", {"class": "lang"})
            percents = soup.findAll("span", {"class": "percent"})

            for id, language in enumerate(languages, start=0):
                language_stats[language.text] = float(
                    percents[id].text.replace('%', ''))

        return language_stats

    def search(self, keywords, _type, extra):

        url_parts = list(urlparse(self.__url_base))
        # Setting search path
        url_parts[2] = 'search'
        # Setting search query params
        query = ' '.join([str(k) for k in keywords])
        search_params = {'q': query, 'type': _type}

        search_response = self.__run('GET', urlunparse(url_parts),
                                     search_params)

        if (_type == 'Repositories' and extra):
            links = self.__extract_links_extra(search_response)
        else:
            links = self.__extract_links(search_response)

        return links
