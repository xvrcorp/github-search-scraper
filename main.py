from lib.github_scraper import GithubScraper
import json
import random


def github_search(input, extra=False):

    proxy = random.choice(input['proxies'])
    scraper = GithubScraper(proxy)

    search_results_links = scraper.search(input['keywords'], input['type'],
                                          extra)

    print('>>> Search results: ')
    print(json.dumps(search_results_links, sort_keys=True, indent=4))


if __name__ == "__main__":

    input = {
        "keywords": ["python", "django-rest-framework", "jwt"],
        "proxies": ["85.235.41.173:3128", "177.130.55.154:36354"],
        "type": "Repositories"
    }

    # Github Search
    github_search(input)

    # Github Search (Extra)
    print('\n Extra > ')
    github_search(input, extra=True)
