import pytest
from lib.github_scraper import GithubScraper
import random
import re

regex = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  #domain...
    r'localhost|'  #localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$',
    re.IGNORECASE)


def test_search_request():
    """Test the search method"""
    input = {
        "keywords": ["openstack", "nova", "css"],
        "proxies": ["85.235.41.173:3128", "177.130.55.154:36354"],
        "type": "Repositories"
    }

    proxy = random.choice(input['proxies'])
    scraper = GithubScraper(proxy)

    links = scraper.search(input['keywords'], input['type'], extra=False)
    assert links == [{
        "url":
        "https://github.com/atuldjadhav/DropBox-Cloud-Storage"
    }]


def test_search_request_extra():
    """Test the search method with extra info"""
    input = {
        "keywords": ["python", "merixstudio"],
        "proxies": ["178.128.54.106:3128"],
        "type": "Repositories"
    }

    proxy = random.choice(input['proxies'])
    scraper = GithubScraper(proxy)

    links = scraper.search(input['keywords'], input['type'], extra=True)

    for link in links:
        assert re.match(regex, link['url']) == True
        assert 'merixstudio' == link['extra']['owner']
        assert hasattr(link['extra']['language_stats'], 'Python') == True
        assert link['extra']['language_stats']['Python'].isnumeric() == True
